/*

*/

angular.module('deepBlue.services', [])

.factory('Loader', ['$ionicLoading', '$timeout', function($ionicLoading, $timeout) {

    var LOADERAPI = {

        showLoading: function(text) {
            text = text || 'Loading...';
            $ionicLoading.show({
                template: text
            });
        },

        hideLoading: function() {
            $ionicLoading.hide();
        },

        toggleLoadingWithMessage: function(text, timeout) {
            var self = this;

            self.showLoading(text);

            $timeout(function() {
                self.hideLoading();
            }, timeout || 3000);
        }

    };
    return LOADERAPI;
}])

.factory('LSFactory', [function() {

    var LSAPI = {

        clear: function() {
            return localStorage.clear();
        },

        get: function(key) {
            return JSON.parse(localStorage.getItem(key));
        },

        add: function(key, data) {
            var oldData = this.get(key);
            if(oldData == null)
            {
                oldData = [];
            }
            oldData.push(data);
            this.set(key, oldData);
        },

        set: function(key, data) {
            return localStorage.setItem(key, JSON.stringify(data));
        },

        delete: function(key) {
            return localStorage.removeItem(key);
        },

        getAll: function() {
            var allData = [];
            var items = Object.keys(localStorage);

            for (var i = 0; i < items.length; i++) {
                if (items[i] !== 'user' || items[i] != 'token') {
                    allData.push(JSON.parse(localStorage[items[i]]));
                }
            }

            return allData;
        }

    };

    return LSAPI;

}])


.factory('AuthFactory', ['LSFactory', function(LSFactory) {

    var userKey = 'user';
    var tokenKey = 'token';

    var AuthAPI = {

        isLoggedIn: function() {
            return this.getUser() === null ? false : true;
        },

        getUser: function() {
            return LSFactory.get(userKey);
        },

        setUser: function(user) {
            return LSFactory.set(userKey, user);
        },

        getToken: function() {
            return LSFactory.get(tokenKey);
        },

        setToken: function(token) {
            return LSFactory.set(tokenKey, token);
        },

        deleteAuth: function() {
            LSFactory.delete(userKey);
            LSFactory.delete(tokenKey);
        }

    };

    return AuthAPI;

}])

.factory('TokenInterceptor', ['$q', 'AuthFactory', function($q, AuthFactory) {

    return {
        request: function(config) {
            config.headers = config.headers || {};
            config.data = config.data || {};
            var token = AuthFactory.getToken();
            var user = AuthFactory.getUser();

            if (token && user && token.token && user.email && config.method == 'POST') {
                config.headers['Content-Type'] = "application/json";
                
                config.data['xToken'] = token.token;
                config.data['xKey'] = user.email;
            }


            return config || $q.when(config);
        },

        response: function(response) {
            return response || $q.when(response);
        }
    };

}])


.factory('UserFactory', ['$http', 'AuthFactory',
    function($http, AuthFactory) {

        var UserAPI = {

            login: function(user) {
                
              var data = $.param({
                username : user.username,
                password : user.password
              });
        
                var config = {
                    headers : {
                        'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                    }
                }
                return $http.post(base + '/controller/rest/api/login/auth', data,config);
            },
            register: function(user) {
                
              var data = $.param({
                username : user.username,
                password : user.password,
                passwordConfirm : user.passwordConfirm,
                name : user.name,
                contact : user.contact
              });
        
                var config = {
                    headers : {
                        'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                    }
                }
                return $http.post(base + '/controller/rest/api/login/register', data,config);
            },


            logout: function() {
                AuthFactory.deleteAuth();
            }

        };

        return UserAPI;
    }
])


.factory('ProductDataFactory', ['$http','LSFactory','AuthFactory', function($http,LSFactory, AuthFactory) {
    

    var API = {
   
    list: function(username) {
        return $http.get(base + '/controller/rest/api/member/productdata/list?username='+username);
    },
    get: function(id) {
        return $http.get(base + '/controller/rest/api/member/productdata/get/'+id);
    },
    update: function(id, input) {
 
         var data = $.param({
                price : input.price,
                date : input.date,
                stock : input.stock,
                'product.id' :input.product.id,
                'market.id' : input.market.id
              });
        
                var config = {
                    headers : {
                        'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                    }
                }

        return $http.post(base + '/controller/rest/api/member/productdata/update/'+id, data,config);
    },



    };

    return API;

}])

.factory('PurchaseOrderFactory', ['$http','LSFactory','AuthFactory', function($http,LSFactory, AuthFactory) {
    

    var API = {
   
    list: function(username) {
        return $http.get(base + '/controller/rest/api/member/purchaseorder/list?username='+username);
    },
    get: function(id) {
        return $http.get(base + '/controller/rest/api/member/purchaseorder/get/'+id);
    },
    add: function(id, input) {
      
         var data = $.param({
                price : input.price,
                date : input.date,
                stock : input.stock,
                notes : input.notes,
                termpayment : input.termpayment,
                'product.id' :input.product.id,
                'market.id' : input.market.id
              });
        
                var config = {
                    headers : {
                        'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                    }
                }
        
        return $http.post(base + '/controller/rest/api/member/purchaseorder/add/'+id, data,config);
    },
    order: function(id,type,input) {
        // type == DROPPED, else APPROVE
        var data = $.param({
                comment : input.comment
              });
        
                var config = {
                    headers : {
                        'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                    }
                }
        return $http.post(base + '/controller/rest/api/member/purchaseorder/order/'+id+'/'+type, data,config);
    },
    finish: function(id,type,input) {
        // type == DROPPED, else APPROVE
        // type == DROPPED, else APPROVE
        var data = $.param({
                comment : input.comment
              });
        
                var config = {
                    headers : {
                        'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                    }
                }
        return $http.post(base + '/controller/rest/api/member/purchaseorder/finish/'+id+'/'+type, data,config);
    },



    };

    return API;

}])


.factory('MasterDataFactory', ['$http','LSFactory','AuthFactory', function($http,LSFactory, AuthFactory) {
    

    var API = {
   
    product: function() {
        return $http.get(base + '/controller/rest/open/masterdata/product/findAll');
    },
    province: function() {
        return $http.get(base + '/controller/rest/open/masterdata/province/findAll');
    },
    city: function(id) {
        return $http.get(base + '/controller/rest/open/masterdata/city/findByProvinceId/'+id);
    },
    market: function(id) {
        return $http.get(base + '/controller/rest/open/masterdata/market/findByCityId/'+id);
    },

    };

    return API;

}])