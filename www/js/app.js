/*

*/
var base = 'http://koppasindo.com';
angular.module('deepBlue', ['ionic', 'deepBlue.controllers', 'deepBlue.services'])

.run(function($ionicPlatform, $rootScope, $timeout, $state) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }

    /* 
      #SIMPLIFIED-IMPLEMENTATION:
      Example access control.
      A real app would probably call a service method to check if there
      is a logged user.

      #IMPLEMENTATION-DETAIL: views that require authorizations have an
      "auth" key with value = "true".
    */
    $rootScope.$on('$stateChangeStart', 
      function(event, toState, toParams, fromState, fromParams){
        if(toState.data && toState.data.auth == true && !$rootScope.user.email){
          event.preventDefault();
          $state.go('app.login');   
        }
    });

  });
})

.run(['$rootScope', 'AuthFactory',
    function($rootScope, AuthFactory) {

        $rootScope.isAuthenticated = AuthFactory.isLoggedIn();
        
    }
])

.config(function($stateProvider, $urlRouterProvider) {

  /*

    Here we setup the views of our app.
    In this case:
    - feed, account, shop, checkout, cart will require login
    - app will go to the "start view" when launched.

    #IMPLEMENTATION-DETAIL: views that require authorizations have an
    "auth" key with value = "true".

  */
  
  $stateProvider

  .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })
  
  .state('app.start', {
    url: '/start',
    views: {
      'menuContent': {
        templateUrl: 'templates/start.html'
      }
    }
  })

  .state('app.login', {
    url: '/login',
    cached : false,
    views: {
      'menuContent': {
        templateUrl: 'templates/login.html',
        controller : 'LoginCtrl'
      }
    }
  })

  .state('app.forgot', {
    url: '/forgot',
    views: {
      'menuContent': {
        templateUrl: 'templates/forgot.html'
      }
    }
  })

  .state('app.signup', {
    url: '/signup',
    views: {
      'menuContent': {
        templateUrl: 'templates/signup.html',
        controller : 'RegisterCtrl'
      }
    }
  })

  
  .state('app.home', {
    url: '/home',
    views: {
      'menuContent': {
                    templateUrl: "templates/view/gallery.html",
                    controller: 'GalleryCtrl'
                }
    }
  })

  .state('app.po', {
    url: '/po',
    cache : false,
    views: {
      'menuContent': {
        templateUrl: 'templates/view/purchaseorder.html',
        controller : 'PurchaseOrderCtrl'
      }
    }
  })
   .state('app.podetail', {
            url: "/podetail/:detailId",
            cache : false,
            views: {
                'menuContent': {
                    templateUrl: "templates/view/purchaseorderdetail.html",
                    controller: 'PurchaseOrderDetailCtrl'
                }
            }
        })

  .state('app.pocreate', {
            url: "/pocreate",
            cache : false,
            views: {
                'menuContent': {
                    templateUrl: "templates/view/purchaseordercreate.html",
                    controller: 'PurchaseOrderCreateCtrl'
                }
            }
        })


  .state('app.updateharga', {
    url: '/updateharga',
    cache : false,
    views: {
      'menuContent': {
        templateUrl: 'templates/view/updateharga.html',
        controller : 'ProductDataCtrl'
      }
    }
  })
  .state('app.updatehargadetail', {
            url: "/updatehargadetail/:detailId",
            views: {
                'menuContent': {
                    templateUrl: "templates/view/updatehargadetail.html",
                    controller: 'ProductDataDetailCtrl'
                }
            }
        })

  .state('app.updatehargacreate', {
            url: "/updatehargacreate",
            cache : false,
            views: {
                'menuContent': {
                    templateUrl: "templates/view/updatehargacreate.html",
                    controller: 'ProductDataCreateCtrl'
                }
            }
        })
.state('app.gallery', {
            url: "/gallery",
            cache : false,
            views: {
                'menuContent': {
                    templateUrl: "templates/view/gallery.html",
                    controller: 'GalleryCtrl'
                }
            }
        })

.state('app.underconstruction', {
            url: "/underconstruction",
            views: {
                'menuContent': {
                    templateUrl: "templates/view/underconstruction.html",
                    controller: 'UnderConstructionCtrl'
                }
            }
        })
  // If none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/start');

});
