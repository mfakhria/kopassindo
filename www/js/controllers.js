/*
  

*/


//controllers are packed into a module
angular.module('deepBlue.controllers', [])

//top view controller
.controller('AppCtrl', function($scope, $rootScope, $state,Loader,UserFactory,AuthFactory) {
  
  $scope.logout = function(){
    UserFactory.logout();
    $rootScope.isAuthenticated = false;
    $state.go('app.start');
    Loader.toggleLoadingWithMessage('Successfully Logged Out!', 2000);
  };


   var token = AuthFactory.getToken();
   var user = AuthFactory.getUser();

    if (token && user && token.token && user.username) {

        $rootScope.isAuthenticated = true;
        $state.go('app.home');
    }


})


.controller('LoginCtrl', function ($scope, $state, $rootScope,$ionicPopup, Loader,UserFactory,AuthFactory) {
  $scope.user = {
                    username: '',
                    password: '',
                    name : '',
                    contact : ''
                };

 
  $scope.login = function() {
      Loader.showLoading('Authenticating...');

      UserFactory.login($scope.user).success(function(data) {

           try{   

              if(data.id)
              {
                  
                  AuthFactory.setUser(data);
                  AuthFactory.setToken({
                      token: data.password,
                  });

                  $rootScope.isAuthenticated = true;

                  
                  Loader.hideLoading();
                  Loader.toggleLoadingWithMessage('Successfully Login!', 2000);
                  $state.go('app.home');

              }else{
                  Loader.hideLoading();
                  $ionicPopup.alert({
                       title: 'error',
                       cssClass: 'alertErr',
                       template: "Username/Password Tidak Sesuai"
                     });
              }
          }catch(e)
          {
              Loader.hideLoading();
                  $ionicPopup.alert({
                       title: 'error code',
                       template: e
                     });
          }

          
      }).error(function(err, statusCode) {
          Loader.hideLoading();
          Loader.toggleLoadingWithMessage(err.message);
      });
  }
  
})


.controller('RegisterCtrl', function ($scope, $state, $rootScope,$ionicPopup, Loader,UserFactory,AuthFactory) {
  $scope.user = {
                    username: '',
                    password: '',
                    passwordConfirm: '',
                };

 
  $scope.register = function() {
     
if($scope.user.password != $scope.user.passwordConfirm)
{
$ionicPopup.alert({
                       title: 'error',
                       cssClass: 'alertErr',
                       template: "Password Tidak Sesuai"
                     });
}else{
   Loader.showLoading('Registering...');
   UserFactory.register($scope.user).success(function(data) {

           try{   

              if(data.id)
              {
                  
                  

                  
                  Loader.hideLoading();
                  Loader.toggleLoadingWithMessage('Successfully Registered!', 2000);
                  $state.go('app.login');

              }else{
                  Loader.hideLoading();
                  $ionicPopup.alert({
                       title: 'error',
                       cssClass: 'alertErr',
                       template: "Username/Password Tidak Sesuai"
                     });
              }
          }catch(e)
          {
              Loader.hideLoading();
                  $ionicPopup.alert({
                       title: 'error code',
                       template: e
                     });
          }

          
      }).error(function(err, statusCode) {
          Loader.hideLoading();
          Loader.toggleLoadingWithMessage(err.message);
      });
}
     
  }
  
})



.controller('ProductDataCtrl', function ($scope, $state, $rootScope,$ionicPopup, Loader,UserFactory,AuthFactory,ProductDataFactory) {
  $scope.list = {};
  $scope.countFetching = 0;
  $scope.user =  AuthFactory.getUser();


  $scope.getList = function()
{
  $scope.countFetching = $scope.countFetching + 1;
  Loader.showLoading('Fetching data...');
   
  ProductDataFactory.list($scope.user.username).success(function(data) {
     try{   

          if(data)
          {
           
            $scope.list = data;
           
              Loader.hideLoading();
              
          }else{
              Loader.hideLoading();
              $ionicPopup.alert({
                   title: 'error ',
                   template: "Tidak dapat mengambil data"
                 });
          }
      }catch(e)
      {
          Loader.hideLoading();
              $ionicPopup.alert({
                   title: 'error code',
                   template: e
                 });
      }
  }).error(function(err, statusCode) {
      Loader.hideLoading();
      Loader.toggleLoadingWithMessage(err.message);
  });
} // CLOSE GET LIST

$scope.getList();
})



.controller('ProductDataDetailCtrl', function ($scope, $state, $rootScope,$ionicPopup, Loader,UserFactory,AuthFactory,ProductDataFactory) {

  var detailId = $state.params.detailId;
  $scope.productData = {};

  $scope.getDetail = function()
{
 
  Loader.showLoading('Fetching data...');
   
  ProductDataFactory.get(detailId).success(function(data) {
     try{   

          if(data)
          {
           
             $scope.productData = data;
              Loader.hideLoading();
              
          }else{
              Loader.hideLoading();
              $ionicPopup.alert({
                   title: 'error ',
                   template: "Tidak dapat mengambil data"
                 });
          }
      }catch(e)
      {
          Loader.hideLoading();
              $ionicPopup.alert({
                   title: 'error code',
                   template: e
                 });
      }
  }).error(function(err, statusCode) {
      Loader.hideLoading();
      Loader.toggleLoadingWithMessage(err.message);
  });
} // CLOSE GET LIST

$scope.getDetail();
})



.controller('ProductDataCreateCtrl', function ($scope, $state, $rootScope,$ionicPopup, Loader,UserFactory,AuthFactory,MasterDataFactory,ProductDataFactory) {

  var user = AuthFactory.getUser();

  $scope.product = {};
  $scope.province = {};
  $scope.city = {};
  $scope.market = {};

  $scope.productData = {
        price : 0,
        date : new Date(),
        stock : 0,
        product :{},
        market : {},
  };
  $scope.productData.product.id = 0;
  $scope.productData.market.id = 0;
  $scope.productData.market.city = {};
  $scope.productData.market.city.id =1;
  $scope.productData.market.city.province = {};
  $scope.productData.market.city.province.id=1;




  $scope.getProduct= function()
{
 
  Loader.showLoading('Fetching data...');
   
  MasterDataFactory.product().success(function(data) {
     try{   

          if(data)
          {
           
             $scope.product = data;
              Loader.hideLoading();
              
          }else{
              Loader.hideLoading();
              $ionicPopup.alert({
                   title: 'error ',
                   template: "Tidak dapat mengambil data"
                 });
          }
      }catch(e)
      {
          Loader.hideLoading();
              $ionicPopup.alert({
                   title: 'error code',
                   template: e
                 });
      }
  }).error(function(err, statusCode) {
      Loader.hideLoading();
      Loader.toggleLoadingWithMessage(err.message);
  });
} // CLOSE GET LIST

$scope.getProvince= function()
{
 
  Loader.showLoading('Fetching data...');
   
  MasterDataFactory.province().success(function(data) {
     try{   

          if(data)
          {
           
             $scope.province = data;
              Loader.hideLoading();
              
          }else{
              Loader.hideLoading();
              $ionicPopup.alert({
                   title: 'error ',
                   template: "Tidak dapat mengambil data"
                 });
          }
      }catch(e)
      {
          Loader.hideLoading();
              $ionicPopup.alert({
                   title: 'error code',
                   template: e
                 });
      }
  }).error(function(err, statusCode) {
      Loader.hideLoading();
      Loader.toggleLoadingWithMessage(err.message);
  });
} // CLOSE GET LIST


$scope.getCity= function()
{
 $scope.city = {};
 $scope.market = {};
  $scope.productData.market.id = 0;
  Loader.showLoading('Fetching data...');
   
  MasterDataFactory.city($scope.productData.market.city.province.id).success(function(data) {
     try{   

          if(data)
          {
           
             $scope.city = data;
              Loader.hideLoading();
              
          }else{
              Loader.hideLoading();
              $ionicPopup.alert({
                   title: 'error ',
                   template: "Tidak dapat mengambil data"
                 });
          }
      }catch(e)
      {
          Loader.hideLoading();
              $ionicPopup.alert({
                   title: 'error code',
                   template: e
                 });
      }
  }).error(function(err, statusCode) {
      Loader.hideLoading();
      Loader.toggleLoadingWithMessage(err.message);
  });
} // CLOSE GET LIST


$scope.getMarket= function()
{
 $scope.market = {};
  $scope.productData.market.id = 0;
 Loader.showLoading('Fetching data...');
   
  MasterDataFactory.market($scope.productData.market.city.id).success(function(data) {
     try{   

          if(data)
          {
           
             $scope.market = data;
              Loader.hideLoading();
              
          }else{
              Loader.hideLoading();
              $ionicPopup.alert({
                   title: 'error ',
                   template: "Tidak dapat mengambil data"
                 });
          }
      }catch(e)
      {
          Loader.hideLoading();
              $ionicPopup.alert({
                   title: 'error code',
                   template: e
                 });
      }
  }).error(function(err, statusCode) {
      Loader.hideLoading();
      Loader.toggleLoadingWithMessage(err.message);
  });
} // CLOSE GET LIST
$scope.getProduct();
$scope.getProvince();

if(user)
{
    if(user.market && user.market.id)
    {
      
      $scope.productData.market.id = user.market.id;
      if(user.market.city && user.market.city.id)
      {
        $scope.productData.market.city.id =user.market.city.id;
        if(user.market.city.province && user.market.city.province.id)
        {
           $scope.productData.market.city.province.id=user.market.city.province.id;
        }
      }
    }
  }

  $scope.getCity();
  if(user)
{
    if(user.market && user.market.id)
    {
      
      $scope.productData.market.id = user.market.id;
      if(user.market.city && user.market.city.id)
      {
        $scope.productData.market.city.id =user.market.city.id;
        if(user.market.city.province && user.market.city.province.id)
        {
           $scope.productData.market.city.province.id=user.market.city.province.id;
        }
      }
    }
  }
  $scope.getMarket();
  if(user)
{
    if(user.market && user.market.id)
    {
      
      $scope.productData.market.id = user.market.id;
      if(user.market.city && user.market.city.id)
      {
        $scope.productData.market.city.id =user.market.city.id;
        if(user.market.city.province && user.market.city.province.id)
        {
           $scope.productData.market.city.province.id=user.market.city.province.id;
        }
      }
    }
  }

$scope.create = function()
{
  if( $scope.productData.market.id == 0)
  {
   $ionicPopup.alert({
                   title: 'error ',
                   cssClass: 'alertErr',
                   template: "Mohon Pilih Pasar"
                 });
  }else if( $scope.productData.product.id == 0)
  {
   $ionicPopup.alert({
                   title: 'error ',
                   cssClass: 'alertErr',
                   template: "Mohon Pilih Komoditas"
                 });
  }else{

     var data = {};
        data.price =$scope.productData.price;
        data.date = $scope.productData.date.getDate() + " " + ($scope.productData.date.getMonth()+1) + " " + $scope.productData.date.getFullYear(); // dd MM yyyy
        data.stock = $scope.productData.stock;
        data.product = {};
        data.product.id = $scope.productData.product.id;
        data.market = {};
        data.market.id = $scope.productData.market.id;
    ProductDataFactory.update(user.id, data).success(function(data) {
     try{   

          if(data && data.id)
          {
           
             
              Loader.hideLoading();
              Loader.toggleLoadingWithMessage('Data Terkirim', 2000);

              $state.go('app.home');
          }else{
              Loader.hideLoading();
              $ionicPopup.alert({
                   title: 'error ',
                   template: "Tidak dapat mengambil data"
                 });
          }
      }catch(e)
      {
          Loader.hideLoading();
              $ionicPopup.alert({
                   title: 'error code',
                   template: e
                 });
      }
  }).error(function(err, statusCode) {
      Loader.hideLoading();
      Loader.toggleLoadingWithMessage(err.message);
  });

  }
}
})


.controller('PurchaseOrderCtrl', function ($scope, $state, $rootScope,$ionicPopup, Loader,UserFactory,AuthFactory,PurchaseOrderFactory) {
  $scope.list = {};
  $scope.countFetching = 0;
  $scope.user =  AuthFactory.getUser();


  $scope.getList = function()
{
  $scope.countFetching = $scope.countFetching + 1;
  Loader.showLoading('Fetching data...');
   
  PurchaseOrderFactory.list($scope.user.username).success(function(data) {
     try{   

          if(data)
          {
           
            $scope.list = data;
           
              Loader.hideLoading();
              
          }else{
              Loader.hideLoading();
              $ionicPopup.alert({
                   title: 'error ',
                   template: "Tidak dapat mengambil data"
                 });
          }
      }catch(e)
      {
          Loader.hideLoading();
              $ionicPopup.alert({
                   title: 'error code',
                   template: e
                 });
      }
  }).error(function(err, statusCode) {
      Loader.hideLoading();
      Loader.toggleLoadingWithMessage(err.message);
  });
} // CLOSE GET LIST

$scope.getList();
})



.controller('GalleryCtrl', function ($scope, $state, $rootScope,$ionicPopup, Loader,UserFactory,AuthFactory,PurchaseOrderFactory) {
$scope.items = [
            {
                color: "#E47500",
                icon: "ion-paper-airplane",
                title: "Purchase Order"
            },
            {
                color: "#3DBEC9",
                icon: "ion-stats-bars",
                title: "Harga Pasar"
            },
            {
                color: "#F8E548",
                icon: "ion-gear-b",
                title: "Settings"
            },
            {
                color: "#AD5CE9",
                icon: "ion-card",
                title: "Fin - Tech"
            },
            {
                color: "#5AD863",
                icon: "ion-clipboard",
                title: "History"
            },
            {
                color: "#D86B67",
                icon: "ion-information-circled",
                title: "Informasi"
            }
        ];

        $scope.openItem = function(item){
          if(item.title == 'Purchase Order')
          {
            $state.go('app.po');
          }else  if(item.title == 'Harga Pasar')
          {
            $state.go('app.updateharga');
          }
          else
          {
            $state.go('app.underconstruction');
          }
            
        };
})


.controller('UnderConstructionCtrl', function ($scope, $state, $rootScope,$ionicPopup, Loader,UserFactory,AuthFactory,PurchaseOrderFactory) {

})



.controller('PurchaseOrderDetailCtrl', function ($scope, $state, $rootScope,$ionicPopup, Loader,UserFactory,AuthFactory,PurchaseOrderFactory) {

  var detailId = $state.params.detailId;
  $scope.po = {};

  $scope.order = function()
  {
    var type = "PURCHASE";

    Loader.showLoading('Fetching data...');
   var input = {};
   input.comment = "";

  PurchaseOrderFactory.order(detailId, type, input).success(function(data) {
     try{   

          if(data)
          {
           
            
           
              Loader.hideLoading();
              $state.go('app.home');
              Loader.toggleLoadingWithMessage('Pembelian berhasil dilakukan', 2000);
          }else{
              Loader.hideLoading();
              $ionicPopup.alert({
                   title: 'error ',
                   template: "Tidak dapat mengambil data"
                 });
          }
      }catch(e)
      {
          Loader.hideLoading();
              $ionicPopup.alert({
                   title: 'error code',
                   template: e
                 });
      }
  }).error(function(err, statusCode) {
      Loader.hideLoading();
      Loader.toggleLoadingWithMessage(err.message);
  });


  }
  $scope.dropped = function()
  {

    $ionicPopup.prompt({
     title: 'Drop PO',
     template: 'Mohon isi komentar alasan Anda',
     inputType: 'text',
     cssClass: 'alertErr',
     inputPlaceholder: 'Alasan'
    }).then(function(res) {
      if(res)
      {
        if(res.length == 0)
        {
            $ionicPopup.alert({
                   title: 'error',
                   cssClass: 'alertErr',
                   template: 'Mohon isi komentar'
                 });
        }else
        {
          var type = "DROPPED";

    Loader.showLoading('Fetching data...');
   var input = {};
   input.comment = res;
   
  PurchaseOrderFactory.order(detailId, type, input).success(function(data) {
     try{   

          if(data)
          {
           
            
             $state.go('app.home');
           
              Loader.hideLoading();
              Loader.toggleLoadingWithMessage('Permintaan Anda dibatalkan', 2000);
          }else{
              Loader.hideLoading();
              $ionicPopup.alert({
                   title: 'error ',
                   template: "Tidak dapat mengambil data"
                 });
          }
      }catch(e)
      {
          Loader.hideLoading();
              $ionicPopup.alert({
                   title: 'error code',
                   template: e
                 });
      }
  }).error(function(err, statusCode) {
      Loader.hideLoading();
      Loader.toggleLoadingWithMessage(err.message);
  });
        }
      }

      

    });

    
  }

  $scope.received = function()
  {
    var type = "PURCHASE";

    Loader.showLoading('Fetching data...');
   var input = {};
   input.comment = "";
   
  PurchaseOrderFactory.finish(detailId, type, input).success(function(data) {
     try{   

          if(data)
          {
           
            Loader.hideLoading();
              $state.go('app.home');
           Loader.toggleLoadingWithMessage('Diterima oleh pedagang', 2000);
             
              
          }else{
              Loader.hideLoading();
              $ionicPopup.alert({
                   title: 'error ',
                   template: "Tidak dapat mengambil data"
                 });
          }
      }catch(e)
      {
          Loader.hideLoading();
              $ionicPopup.alert({
                   title: 'error code',
                   template: e
                 });
      }
  }).error(function(err, statusCode) {
      Loader.hideLoading();
      Loader.toggleLoadingWithMessage(err.message);
  });

  }

  $scope.returned = function()
  {

   $ionicPopup.prompt({
     title: 'Drop PO',
     template: 'Mohon isi komentar alasan Anda',
     inputType: 'text',
     cssClass: 'alertErr',
     inputPlaceholder: 'Alasan'
    }).then(function(res) {
   

if(res)
      {
        if(res.length == 0)
        {
            $ionicPopup.alert({
                   title: 'error',
                   cssClass: 'alertErr',
                   template: 'Mohon isi komentar'
                 });
        }else
        {
   var type = "DROPPED";
    Loader.showLoading('Fetching data...');
   var input = {};
   input.comment = res;
   
  PurchaseOrderFactory.finish(detailId, type, input).success(function(data) {
     try{   

          if(data)
          {
           
          Loader.hideLoading();
              $state.go('app.home');
           Loader.toggleLoadingWithMessage('Dikembalikan oleh Pedagang', 2000);
           
              
          }else{
              Loader.hideLoading();
              $ionicPopup.alert({
                   title: 'error ',
                   template: "Tidak dapat mengambil data"
                 });
          }
      }catch(e)
      {
          Loader.hideLoading();
              $ionicPopup.alert({
                   title: 'error code',
                   template: e
                 });
      }
  }).error(function(err, statusCode) {
      Loader.hideLoading();
      Loader.toggleLoadingWithMessage(err.message);
  });
}
}
 });

    

  }

  $scope.getDetail = function()
{
 
  Loader.showLoading('Fetching data...');
   
  PurchaseOrderFactory.get(detailId).success(function(data) {
     try{   

          if(data)
          {
           
             $scope.po = data;
              Loader.hideLoading();
              
          }else{
              Loader.hideLoading();
              $ionicPopup.alert({
                   title: 'error ',
                   template: "Tidak dapat mengambil data"
                 });
          }
      }catch(e)
      {
          Loader.hideLoading();
              $ionicPopup.alert({
                   title: 'error code',
                   template: e
                 });
      }
  }).error(function(err, statusCode) {
      Loader.hideLoading();
      Loader.toggleLoadingWithMessage(err.message);
  });
} // CLOSE GET LIST

$scope.getDetail();
})



.controller('PurchaseOrderCreateCtrl', function ($scope, $state, $rootScope,$ionicPopup, Loader,UserFactory,AuthFactory,MasterDataFactory,PurchaseOrderFactory) {

  var user = AuthFactory.getUser();

  $scope.product = {};
  $scope.province = {};
  $scope.city = {};
  $scope.market = {};

  $scope.po = {
        price : 0,
        date : new Date(),
        stock : 0,
        notes : "",
        termpayment : "",
        product :{},
        market : {},
  };
 
  $scope.po.product.id = 0;
  $scope.po.market.id = 0;
  $scope.po.market.city = {};
  $scope.po.market.city.id =1;
  $scope.po.market.city.province = {};
  $scope.po.market.city.province.id=1;




  $scope.getProduct= function()
{
 
  Loader.showLoading('Fetching data...');
   
  MasterDataFactory.product().success(function(data) {
     try{   

          if(data)
          {
           
             $scope.product = data;
              Loader.hideLoading();
              
          }else{
              Loader.hideLoading();
              $ionicPopup.alert({
                   title: 'error ',
                   template: "Tidak dapat mengambil data"
                 });
          }
      }catch(e)
      {
          Loader.hideLoading();
              $ionicPopup.alert({
                   title: 'error code',
                   template: e
                 });
      }
  }).error(function(err, statusCode) {
      Loader.hideLoading();
      Loader.toggleLoadingWithMessage(err.message);
  });
} // CLOSE GET LIST

$scope.getProvince= function()
{
 
  Loader.showLoading('Fetching data...');
   
  MasterDataFactory.province().success(function(data) {
     try{   

          if(data)
          {
           
             $scope.province = data;
              Loader.hideLoading();
              
          }else{
              Loader.hideLoading();
              $ionicPopup.alert({
                   title: 'error ',
                   template: "Tidak dapat mengambil data"
                 });
          }
      }catch(e)
      {
          Loader.hideLoading();
              $ionicPopup.alert({
                   title: 'error code',
                   template: e
                 });
      }
  }).error(function(err, statusCode) {
      Loader.hideLoading();
      Loader.toggleLoadingWithMessage(err.message);
  });
} // CLOSE GET LIST


$scope.getCity= function()
{
 $scope.city = {};
 $scope.market = {};
  $scope.po.market.id = 0;
  Loader.showLoading('Fetching data...');
   
  MasterDataFactory.city($scope.po.market.city.province.id).success(function(data) {
     try{   

          if(data)
          {
           
             $scope.city = data;
              Loader.hideLoading();
              
          }else{
              Loader.hideLoading();
              $ionicPopup.alert({
                   title: 'error ',
                   template: "Tidak dapat mengambil data"
                 });
          }
      }catch(e)
      {
          Loader.hideLoading();
              $ionicPopup.alert({
                   title: 'error code',
                   template: e
                 });
      }
  }).error(function(err, statusCode) {
      Loader.hideLoading();
      Loader.toggleLoadingWithMessage(err.message);
  });
} // CLOSE GET LIST


$scope.getMarket= function()
{
 $scope.market = {};
  $scope.po.market.id = 0;
 Loader.showLoading('Fetching data...');
   
  MasterDataFactory.market($scope.po.market.city.id).success(function(data) {
     try{   

          if(data)
          {
           
             $scope.market = data;
              Loader.hideLoading();
              
          }else{
              Loader.hideLoading();
              $ionicPopup.alert({
                   title: 'error ',
                   template: "Tidak dapat mengambil data"
                 });
          }
      }catch(e)
      {
          Loader.hideLoading();
              $ionicPopup.alert({
                   title: 'error code',
                   template: e
                 });
      }
  }).error(function(err, statusCode) {
      Loader.hideLoading();
      Loader.toggleLoadingWithMessage(err.message);
  });
} // CLOSE GET LIST
$scope.getProduct();
$scope.getProvince();

if(user)
{
    if(user.market && user.market.id)
    {
      
      $scope.po.market.id = user.market.id;
      if(user.market.city && user.market.city.id)
      {
        $scope.po.market.city.id =user.market.city.id;
        if(user.market.city.province && user.market.city.province.id)
        {
           $scope.po.market.city.province.id=user.market.city.province.id;
        }
      }
    }
  }

  $scope.getCity();
  if(user)
{
    if(user.market && user.market.id)
    {
      
      $scope.po.market.id = user.market.id;
      if(user.market.city && user.market.city.id)
      {
        $scope.po.market.city.id =user.market.city.id;
        if(user.market.city.province && user.market.city.province.id)
        {
           $scope.po.market.city.province.id=user.market.city.province.id;
        }
      }
    }
  }
  $scope.getMarket();
  if(user)
{
    if(user.market && user.market.id)
    {
      
      $scope.po.market.id = user.market.id;
      if(user.market.city && user.market.city.id)
      {
        $scope.po.market.city.id =user.market.city.id;
        if(user.market.city.province && user.market.city.province.id)
        {
           $scope.po.market.city.province.id=user.market.city.province.id;
        }
      }
    }
  }

$scope.create = function()
{
  if( $scope.po.market.id == 0)
  {
   $ionicPopup.alert({
                   title: 'error ',
                   cssClass: 'alertErr',
                   template: "Mohon Pilih Pasar"
                 });
  }else if( $scope.po.product.id == 0)
  {
   $ionicPopup.alert({
                   title: 'error ',
                   cssClass: 'alertErr',
                   template: "Mohon Pilih Komoditas"
                 });
  }else{

     var data = {};
        data.price =$scope.po.price;
        data.date = $scope.po.date.getDate() + " " + ($scope.po.date.getMonth()+1) + " " + $scope.po.date.getFullYear(); // dd MM yyyy
        data.stock = $scope.po.stock;
        data.product = {};
        data.product.id = $scope.po.product.id;
        data.market = {};
        data.market.id = $scope.po.market.id;
        data.notes = $scope.po.notes;
        data.termpayment = $scope.po.termpayment;

    PurchaseOrderFactory.add(user.id, data).success(function(data) {
     try{   

          if(data && data.id)
          {
           
             
              Loader.hideLoading();
              Loader.toggleLoadingWithMessage('Data Terkirim', 2000);

              $state.go('app.home');
          }else{
              Loader.hideLoading();
              $ionicPopup.alert({
                   title: 'error ',
                   template: "Tidak dapat mengambil data"
                 });
          }
      }catch(e)
      {
          Loader.hideLoading();
              $ionicPopup.alert({
                   title: 'error code',
                   template: e
                 });
      }
  }).error(function(err, statusCode) {
      Loader.hideLoading();
      Loader.toggleLoadingWithMessage(err.message);
  });

  }
}
})
